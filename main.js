let userData = {
        USD: 1000,
        EUR: 900,
        UAH: 15000,
        BIF: 20000,
        AOA: 100,
    },
    bankData = {
        USD: {
            max: 3000,
            min: 100,
            img: '💵',
        },
        EUR: {
            max: 1000,
            min: 50,
            img: '💶',
        },
        UAH: {
            max: 0,
            min: 0,
            img: '💴',
        },
        GBP: {
            max: 10000,
            min: 100,
            img: '💷',
        },
    };

function getMoney(userData, bankData) {
    return new Promise((resolve, reject) => {
        confirm('Would you want to see the card balance?')
            ? resolve(userData)
            : reject({ userData: userData, bankData: bankData });
    });
}

getMoney(userData, bankData)
    .then(
        agree => {
            let currency = getCurrency(agree);
            console.log(`${agree[currency]} ${currency}`);
        },
        disagree => {
            let currency = getCurrency(disagree.bankData);
            let getCash = +prompt('Enter the amount you want to receive');

            getCash > disagree.bankData[currency].max
                ? console.log(`You can't get more than ${disagree.bankData[currency].max}`)
                : getCash < disagree.bankData[currency].min
                ? console.log(`You can't get less than ${disagree.bankData[currency].min}`)
                : console.log(`Yours money ${getCash} ${currency}`);
        }
    )
    .catch(err => console.log(err))
    .finally(() => console.log('Thanks, have a good day!'));

function getCurrency(obj) {
    let currency;
    do {
        currency = prompt(`Set the currency ${Object.keys(obj).join(' ')}`).toUpperCase();
    } while (!validation(currency, obj));

    return currency;
}

function validation(value, obj) {
    return Object.keys(obj).includes(value);
}